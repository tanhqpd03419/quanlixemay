/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Administrator
 */
public class XeTest {
    
    public XeTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of getMaXe method, of class Xe.
     */
    @Test
    public void testGetMaXe() {
//        System.out.println("getMaXe");
        Xe instance = new Xe();
        String expResult = "";
        String result = instance.getMaXe();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaXe method, of class Xe.
     */
    @Test
    public void testSetMaXe() {
//        System.out.println("setMaXe");
        String maXe = "X01";
        Xe instance = new Xe();
        instance.setMaXe(maXe);
        // TODO review the generated test code and remove the default call to fail.
        String expected="X01";
        assertEquals(expected, instance.getMaXe());
    }

    /**
     * Test of getTenXe method, of class Xe.
     */
    @Test
    public void testGetTenXe() {
//        System.out.println("getTenXe");
        Xe instance = new Xe();
        String expResult = "";
        String result = instance.getTenXe();
        assertEquals(expResult, result);
    
    }

    /**
     * Test of setTenXe method, of class Xe.
     */
    @Test
    public void testSetTenXe() {
//        System.out.println("setTenXe");
        String tenXe = "AB";
        Xe instance = new Xe();
        instance.setTenXe(tenXe);
        String expected="AB";
        assertEquals(expected, instance.getTenXe());
    }

    /**
     * Test of getMaMau method, of class Xe.
     */
    @Test
    public void testGetMaMau() {
       // System.out.println("getMaMau");
        Xe instance = new Xe();
        String expResult = "";
        String result = instance.getMaMau();
        assertEquals(expResult, result);
    
    }

    /**
     * Test of setMaMau method, of class Xe.
     */
    @Test
    public void testSetMaMau() {
      //  System.out.println("setMaMau");
        String maMau = "Do";
        Xe instance = new Xe();
        instance.setMaMau(maMau);
        String expected="Do";
        assertEquals(expected, instance.getMaMau());
    }

    /**
     * Test of getMaHang method, of class Xe.
     */
    @Test
    public void testGetMaHang() {
       // System.out.println("getMaHang");
        Xe instance = new Xe();
        String expResult = "";
        String result = instance.getMaHang();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setMaHang method, of class Xe.
     */
    @Test
    public void testSetMaHang() {
        System.out.println("setMaHang");
        String maHang = "Honda";
        Xe instance = new Xe();
        instance.setMaHang(maHang);
       String expected="Honda";
        assertEquals(expected, instance.getMaHang());
    }

    /**
     * Test of getSoLuong method, of class Xe.
     */
    @Test
    public void testGetSoLuong() {
        //System.out.println("getSoLuong");
        Xe instance = new Xe();
        int expResult = 0;
        int result = instance.getSoLuong();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setSoLuong method, of class Xe.
     */
    @Test
    public void testSetSoLuong() {
        //System.out.println("setSoLuong");
        int soLuong = 10;
        Xe instance = new Xe();
        instance.setSoLuong(soLuong);
        int expected= 10;
        int result = instance.getSoLuong();
        assertEquals(expected, result);
    }
      @Test
    public void testSetSoLuongWithNegative() {
        //System.out.println("setSoLuong");
        int soLuong = -10;
        Xe instance = new Xe();
      Exception exception =assertThrows(Exception.class,
                ()->instance.setSoLuong(soLuong));
      
    }

    /**
     * Test of getGiaNhap method, of class Xe.
     */
    @Test
    public void testGetGiaNhap() {
       // System.out.println("getGiaNhap");
        Xe instance = new Xe();
        double expResult = 0.0;
        double result = instance.getGiaNhap();
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of setGiaNhap method, of class Xe.
     */
    @Test
    public void testSetGiaNhap() {
        //System.out.println("setGiaNhap");
        double giaNhap = 500;
        Xe instance = new Xe();
        instance.setGiaNhap(giaNhap);
        double expected = 500;
        double result= instance.getGiaNhap();
        assertEquals(expected,result,0.0);
       
    }
      public void testSetGiaNhapWithNegative() {
      
        double gianhap = -500;
        Xe instance = new Xe();
        
        Exception exception =assertThrows(Exception.class,
                ()->instance.setGiaNhap(gianhap));
       
    }
    @Test
    public void testSetGiaNhapLarge() {
      
        double giaNhap= 5000000000000d;
        Xe instance = new Xe();
        
        Exception exception =assertThrows(Exception.class,
                ()->instance.setGiaNhap(giaNhap));
       
    }

    /**
     * Test of getGiaBan method, of class Xe.
     */
    @Test
    public void testGetGiaBan() {
        System.out.println("getGiaBan");
        Xe instance = new Xe();
        double expResult = 0.0;
        double result = instance.getGiaBan();
        assertEquals(expResult, result, 0.0);
      
    }

    /**
     * Test of setGiaBan method, of class Xe.
     */
    @Test
    public void testSetGiaBan() {
       // System.out.println("setGiaBan");
        double giaBan = 700;
        Xe instance = new Xe();
        instance.setGiaBan(giaBan);
        double expected = 700;
        double result= instance.getGiaBan();
        assertEquals(expected,result,0.0);
    }
    @Test
    public void testSetGiaBanWithNegative() {
      
        double giaBan = -700;
        Xe instance = new Xe();
        
        Exception exception =assertThrows(Exception.class,
                ()->instance.setGiaBan(giaBan));
       
    }
    @Test
    public void testSetGiaBanLarge() {
      
        double giaBan = 8000000000000d;
        Xe instance = new Xe();
        
        Exception exception =assertThrows(Exception.class,
                ()->instance.setGiaBan(giaBan));
       
    }

    /**
     * Test of getHinh method, of class Xe.
     */
    @Test
    public void testGetHinh() {
//        System.out.println("getHinh");
        Xe instance = new Xe();
        String expResult = "";
        String result = instance.getHinh();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setHinh method, of class Xe.
     */
    @Test
    public void testSetHinh() {
        System.out.println("setHinh");
        String hinh = "icon.png";
        Xe instance = new Xe();
        instance.setHinh(hinh);
           String expected="icon.png";
        assertEquals(expected, instance.getHinh());
        
    }
    
}
