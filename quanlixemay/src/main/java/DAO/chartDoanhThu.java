package DAO;


import DAO.ThongKeDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import utils.JdbcHelper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */

public class chartDoanhThu {
    static Integer year;

    public static Integer getYear() {
        return year;
    }

    public static void setYear(Integer year) {
        chartDoanhThu.year = year;
    }
    
    static ThongKeDAO dao = new ThongKeDAO();

    public static JFreeChart createChart() {
        JFreeChart barChart = ChartFactory.createBarChart(
                "Doanh thu "+ year,
                "Tháng", "Doanh thu",
                createDataset());
        return barChart;
    }

    private static CategoryDataset createDataset() {
        try {
            final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            ResultSet rs = JdbcHelper.query("{CALL sp_doanhthutheonam(?)}",year);
            while (rs.next()) {
                dataset.addValue(rs.getDouble(2), "Đơn vị : kVND", "Tháng "+rs.getString(1));                
            }
            return dataset;
        } catch (SQLException ex) {
            Logger.getLogger(chartDoanhThu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    

    public static void run() {
        ChartPanel chartPanel = new ChartPanel(createChart());
        chartPanel.setPreferredSize(new java.awt.Dimension(560*2, 367*2));        
        JDialog frame = new JDialog();
        frame.add(chartPanel);
        frame.setTitle("Thống kê doanh thu theo tháng trong năm "+ year);
        frame.setSize(1200, 800);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }
    

}
