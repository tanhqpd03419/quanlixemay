/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import utils.JdbcHelper;

/**
 *
 * @author ADMIN
 */
public class ThongKeDAO {
    // Việt
    public List<Object[]> getDoanhThuTheoNam(Integer year) {
        List<Object[]> list = new ArrayList<>();
        try {
            ResultSet rs = null;
            try {
                String sql = "{CALL sp_doanhthutheonam(?)}";
                rs = JdbcHelper.query(sql, year);
                while (rs.next()) {
                    Object[] model
                            = {rs.getString(1),
                                rs.getDouble(2)};
                    list.add(model);
                }
            } finally {
                rs.getStatement().getConnection().close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return list;
    }
        public Double getDoanhThuTheoKhoangThoiGian(Date fromDate, Date toDate) {
        try {
            ResultSet rs = null;
            try {
                String sql = "{CALL sp_doanhthutheokhoangthoigian(?,?)}";
                rs = JdbcHelper.query(sql, fromDate,toDate);
                while (rs.next()) {
                        return rs.getDouble(1);
                }
            } finally {
                rs.getStatement().getConnection().close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return null;

    }
        
        //Quốc
        private List<Object[]> getLisstOfArray(String sql, String[] cols,Object...args){
        try{
            List< Object[]> list = new ArrayList<>();
            ResultSet rs = JdbcHelper.query(sql, args);
            while(rs.next()){
                Object[] vals = new Object[cols.length];
                for(int i=0; i<cols.length;i++){
                    vals[i]= rs.getObject(cols[i]);
                }
                list.add(vals);
            }
            rs.getStatement().getConnection().close();
            return list;
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }
    public List<Object[]> getTongTien(){
        String sql = "{call TK_DoanhthuKH}";
        String cols[]={"makh","hoten","Tongtien","ngayxuat"};
        return this.getLisstOfArray(sql, cols);
    }
    public List<Object[]> getDoanhThu(int thang, int nam){
        String sql = "{call TK_TheoThang(?,?)}";
        String[] cols = {"makh","hoten","tongtien","ngayxuat"};
        return this.getLisstOfArray(sql, cols, thang, nam);
    }
    
    public List<Object[]> getTable(){
        String sql = "{call TK_KH}";
        String cols[]={"makh","hoten","Tongtien","ngayxuat"};
        return this.getLisstOfArray(sql, cols);
    }
    
    // Tân
    public List<Object[]> getsoluongxe(Integer nam,Integer thang) {
        List<Object[]> list = new ArrayList<>();
        try {
            ResultSet rs = null;
            try {
                String sql = "{CALL sp_soluongxe(?,?)}";
                rs = JdbcHelper.query(sql,nam,thang);
                while (rs.next()) {
                    Object[] model
                            = {
                                rs.getString(1),
                                rs.getInt(2),
                                rs.getDouble(3),
                               
                            };
                    list.add(model);
                }
            } finally {
                rs.getStatement().getConnection().close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return list;
    }
    
    // Nguyên
     public List<Object[]> getNhapVaoSoLuong(Integer nam ,Integer thang) {
        List<Object[]> list = new ArrayList<>();
        try {
            ResultSet rs = null;
            try {
                String sql = "{CALL sp_soluongnhap(?,?)}";
                rs = JdbcHelper.query(sql,nam,thang);
                while (rs.next()) {
                    Object[] model
                            = {rs.getString(1),
                                rs.getDouble(2),
                                rs.getDouble(3)};
                    list.add(model);
                }
            } finally {
                rs.getStatement().getConnection().close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return list;
    };
     //cường
     public List<Object[]> gettongtien(Integer nam){
List<Object[]> list = new ArrayList<>();
        try {
            ResultSet rs = null;
            try {
                String sql = "{call tongtien(?)}" ;
                rs = JdbcHelper.query(sql,nam);
                while (rs.next()) {
                    Object[] model
                            = {
                                rs.getString(1),
                                rs.getString(2),
                                rs.getDouble(3),
                                rs.getInt(4)
                               
                            };
                    list.add(model);
                }
            } finally {
                rs.getStatement().getConnection().close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        return list;
    }
    ;
}
